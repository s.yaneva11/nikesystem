package system.coordinates;

public class Coordinates {
    
    private  double x;
    private  double y;
    
    public Coordinates() {
        this.x = 0;
        this.y = 0;
    }
    
    public Coordinates(double x, double y) {
        super();
        this.x = x;
        this.y = y;
    }
    
    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
}
