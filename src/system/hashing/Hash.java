package hashing;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class Hash {

    private final MessageDigest digest;
    private static final String saltPassword = "password";

    public Hash() {
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public String getHash(String password) {
        password += saltPassword;
        byte[] hashed = digest.digest(password.getBytes());
        String result = DatatypeConverter.printHexBinary(hashed);

        return result;
    }

}
