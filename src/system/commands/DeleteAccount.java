package commands;

import java.util.ArrayList;
import java.util.Scanner;

public class DeleteAccount extends Command {

    public DeleteAccount() {

    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {

        System.out.println(
                "Are you sure you want to delete your account? After this, you will not be able to restore it. (y/n ?)");
        @SuppressWarnings("resource")
        Scanner input = new Scanner(System.in);
        String response = input.nextLine();

        if (response.equals("y")) {
            system.users.remove(system.getCurrentUser().getUsername());
            System.out.println("User deleted successfully");
        }
    }
}
