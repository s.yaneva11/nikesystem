package system.commands;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import system.coordinates.Coordinates;
import system.db.DataBase;
import system.factory.Factory;
import system.factory.Product;
import system.factory.Store;
import system.hashing.Hash;
import system.users.User;

public class NikeSystem {

    protected HashMap<String, User> users;
    protected HashMap<String, Factory> factories;
    protected HashMap<String, Product> products;
    protected ArrayList<Store> stores;
    private User currentUser;
    protected DataBase db;

    public NikeSystem() {
        users = new HashMap<>();
        factories = new HashMap<>();
        products = new HashMap<>();
        stores = new ArrayList<>();
        db = new DataBase();
        currentUser = new User();
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void run(ArrayList<String> argv) {
        Command c = null;
        if (argv.get(0).equals("signUp")) {

            c = new Register(argv.get(1), argv.get(2), argv.get(3));

        } else if (argv.get(0).equals("signIn")) {

            c = new LogIn(argv.get(1), argv.get(2));

        } else if (argv.get(0).equals("deleteMyProfile")) {

            c = new DeleteAccount();

        } else if (argv.get(0).equals("show")) {

            c = new Show(argv.get(1));

        } else if (argv.get(0).equals("logout")) {

            c = new LogOut();

        } else if (argv.get(0).equals("exit")) {

            c = new Exit();

        } else if ((argv.get(0).equals("add")) && (argv.get(1).equals("factory"))) {

            c = new AddFactory(argv.get(2));

        } else if ((argv.get(0).equals("set")) && (argv.get(1).equals("production"))) {

            c = new SetProduction(argv.get(2), argv.get(3));

        } else if ((argv.get(0).equals("add")) && (argv.get(1).equals("product"))) {

            c = new AddProduct(argv.get(2), argv.get(3));

        } else if ((argv.get(0).equals("search")) && (argv.get(1).equals("online"))) {

            c = new SearchProduct(argv.get(2));

        } else if ((argv.get(0).equals("add")) && (argv.get(1).equals("store"))) {

            c = new AddStore(argv.get(2));

        } else if (argv.get(0).equals("buy")) {

            c = new BuyProduct(argv.get(1));

        } else if ((argv.get(0).equals("set")) && (argv.get(1).equals("location"))) {

            c = new SetLocation(argv.get(2), argv.get(3));

        }
        
        else if((argv.get(0).equals("search")) && (argv.get(2).equals("closest-store"))) {
            
            c = new ClosestStore(argv.get(1));
            
        }
        c.runCommand(argv, this);
    }

    public boolean userExists(String username) {
        for (String key : users.keySet()) {
            if (key.equals(username)) {
                return true;
            }
        }
        return false;
    }

    public boolean factoryExists(String name) {
        for (String key : factories.keySet()) {
            if (key.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean productExists(String name) {
        for (String key : products.keySet()) {
            if (key.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean storeExists(String name) {
        for (int i = 0; i < stores.size(); i++) {
            if (stores.get(i).getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public User getCurentUser(String username) {
        User result = new User();

        for (String key : users.keySet()) {
            if (key.equals(username)) {
                result = users.get(key);
            }
        }

        return result;
    }

    public Factory getFactory(String name) {
        Factory result = new Factory();

        for (String key : factories.keySet()) {
            if (key.equals(name)) {
                result = factories.get(key);
            }
        }

        return result;
    }

    public Product getProduct(String name) {
        Product result = new Product();

        for (String key : products.keySet()) {
            if (key.equals(name)) {
                result = products.get(key);
            }
        }

        return result;
    }

    public Store getStore(String name) {
        Store result = new Store();

        for (int i = 0; i < stores.size(); i++) {
            if (stores.get(i).getName().equals(name)) {
                result = stores.get(i);
            }
        }

        return result;
    }
    
    public Store getStoreByCoordinates(double x, double y) {
        Store result = new Store();

        for (int i = 0; i < stores.size(); i++) {
            if((stores.get(i).getCoordinates().getX() == x) && (stores.get(i).getCoordinates().getY() == y)) {
                result = stores.get(i);
            }
            else {
                throw new IllegalArgumentException("Store with such coordinates does not exist.");
            }
        }

        return result;
    }

    public void addProductToFactories(Product p) {
        for (String key : factories.keySet()) {
            for (int i = 0; i < 3; i++) {
                factories.get(key).addProducts(p);
            }
        }
    }

    public void addProduct(Product p) {
        products.put(p.getName(), p);
    }
    
    /*public boolean getStoreCoordinates(Coordinates c) {
        for(int i = 0; i < stores.size(); i++) {
            if( (stores.get(i).getCoordinates().getX() > stores.get(i).getCoordinates().getY()) && 
                 stores.get(i).getCoordinates().getY() > stores.get(i).getCoordinates().getY()) {
                
            }
        }
    }*/
    
    /*public void func() {
        Coordinates[] arr;
        List<Integer> list = Arrays.stream(stores.toArray(stores.)))
        List<Integer> listt = ((IntStream) Arrays.stream(arr)).boxed().collect(Collectors.toList());
    }*/
    
    public double[] getXCoordinates() {
        double[] arrRes = new double[100];
        
        for(int i = 0; i < stores.size(); i++) {
            arrRes[i] = stores.get(i).getCoordinates().getX();
        }
        
        return arrRes;
    }
    
    public double[] getYCoordinates() {
        double[] arrRes = new double[100];
        
        for(int i = 0; i < stores.size(); i++) {
            arrRes[i] = stores.get(i).getCoordinates().getY();
        }
        
        return arrRes;
    }
    
    

    public void writeToFile(DataBase db) {
        String filename = db.getFilename();
        try (PrintWriter pw = new PrintWriter(new FileOutputStream(filename))) {
            for (String key : users.keySet()) {
                pw.print(users.get(key).getUsername() + " ");
                Hash hashed = new Hash();
                String hashedPassword = hashed.getHash(users.get(key).getPassword());
                pw.print(hashedPassword + " ");
                pw.println(users.get(key).getTypeOfUser());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
