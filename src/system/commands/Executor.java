package commands;

import java.util.ArrayList;

public class Executor {

    public ArrayList<String> splitInput(String text) {

        String[] input = text.split(" ");
        ArrayList<String> validStrings = new ArrayList<>();

        for (String item : input) {
            if (item.length() > 0) {
                validStrings.add(item);
            }
        }

        return validStrings;
    }
}
