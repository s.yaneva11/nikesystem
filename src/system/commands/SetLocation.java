package system.commands;

import java.util.ArrayList;

import system.coordinates.Coordinates;
import system.factory.Store;
import system.users.User;

public class SetLocation extends Command {
    
    private String x;
    private String y;
    
    public SetLocation(String x, String y) {
        super();
        this.x = x;
        this.y = y;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {
        
        double x = Double.parseDouble(this.x);
        double y = Double.parseDouble(this.y);
        
        Coordinates c = new Coordinates();
        c.setX(x);
        c.setY(y);
        
        if(args.size() == 4) {
            
            User currentUser = new User();
            currentUser = system.getCurrentUser();
            
            currentUser.setCoordinates(c);
            System.out.println("You have set your coordinates successfully.");
        }
        
        else if(args.size() == 5) {
            
            String storeName = args.get(4);
            boolean flag = system.storeExists(storeName);
            
            if(flag) {
                Store s = new Store();
                s = system.getStore(storeName);
                
                s.setCoordinates(c);
            }
            else {
                throw new IllegalArgumentException("No such store!");
            }
            System.out.println("You have set the store's coordinates successfully.");
        }
    }
    
    
    
    
}
