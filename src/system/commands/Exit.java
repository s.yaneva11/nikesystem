package commands;

import java.util.ArrayList;

import db.DataBase;

public class Exit extends Command {

    private DataBase db;

    public Exit() {
        db = new DataBase();
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {
        system.writeToFile(db);
        System.exit(0);
    }

}
