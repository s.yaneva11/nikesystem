package commands;

import java.util.ArrayList;

import factory.Product;
import factory.Sport;

public class AddProduct extends Command {

    private String name;
    private String type;

    public AddProduct(String name, String type) {
        super();
        this.name = name;
        this.type = type;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {
        Product newProduct = new Product();

        boolean flag = system.productExists(name);

        if (flag) {
            throw new IllegalArgumentException("Product with such name already exists!");
        }
        newProduct.setName(args.get(2));
        
        switch (args.get(3)) {
        case "football": {
            newProduct.setSport(Sport.FOOTBALL);
            break;
        }
        case "volleyball": {
            newProduct.setSport(Sport.VOLLEYBALL);
            break;
        }
        case "tennis": {
            newProduct.setSport(Sport.TENNIS);
            break;
        }
        case "all": {
            newProduct.setSport(Sport.ALL);
            break;
        }
        }
        
        system.addProduct(newProduct);
        System.out.println("Product added successfully.");
        
        system.addProductToFactories(newProduct);

    }
    


}
