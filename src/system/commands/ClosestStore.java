package system.commands;

import java.util.ArrayList;

import system.factory.Product;
import system.factory.Store;

public class ClosestStore extends Command {

    private String productName;
    
    public ClosestStore(String productName) {
        super();
        this.productName = productName;
    }


    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {

        boolean flag = system.productExists(productName);
        
        if(flag) {
            Product productToSearch = new Product();
            productToSearch = system.getProduct(productName);
            
            system.getCurrentUser().getCoordinates();
            
            double [] arrX = system.getXCoordinates();
            double [] arrY = system.getYCoordinates();
            
            double userX = system.getCurrentUser().getCoordinates().getX();
            double userY = system.getCurrentUser().getCoordinates().getY();
            
            double closestX = getClosest(arrX, userX);
            double closestY = getClosest(arrY, userY);
            
            Store closestStore = new Store();
            closestStore = system.getStoreByCoordinates(closestX, closestY);
            
            boolean existsInStore = closestStore.contains(productToSearch);
            
            if(existsInStore) {
                System.out.println("Search results: ");
                System.out.println(productToSearch.getName() + " " + productToSearch.getSport());
            }
            else {
                throw new IllegalArgumentException("No such product in the closest store!");
            }
        }
        else {
            throw new IllegalArgumentException("No such product!");
        }
        
    }
    
    public double getClosest(double [] arr, double x) {
        double res = 0;
        double distance = Math.abs(arr[0] - x);
        int idx = 0;
        
        for(int i = 0; i < arr.length; i++) {
            double cdistance = Math.abs(arr[i] - x);
            if(cdistance < distance){
                idx = i;
                distance = cdistance;
            }
        }
        res = arr[idx];
        return res;
    }

    
}
