package commands;

import java.util.ArrayList;

import factory.Product;

public class SearchProduct extends Command {

    private String name;

    public SearchProduct(String name) {
        super();
        this.name = name;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {

        boolean flag = system.productExists(name);

        if (flag) {
            Product result = new Product();
            result = system.getProduct(name);
            System.out.println("Search results: ");
            System.out.println(result.getName() + " " + result.getSport());
        } else {
            throw new IllegalArgumentException("Could not find such product!");
        }

    }

}
