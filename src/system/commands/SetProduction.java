package commands;

import java.util.ArrayList;

import factory.Factory;
import factory.Sport;

public class SetProduction extends Command {

    private String typeOfProduction;
    private String factoryName;

    public SetProduction(String typeOfProduction, String factoryName) {
        super();
        this.typeOfProduction = typeOfProduction;
        this.factoryName = factoryName;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {

        boolean flag = system.factoryExists(factoryName);

        if (!flag) {
            throw new IllegalArgumentException("Such factory does not exist!");
        }

        Factory toSet = new Factory();
        toSet = system.getFactory(factoryName);

        switch (args.get(2)) {
        case "football": {
            toSet.setProductionType(Sport.FOOTBALL);
            break;
        }
        case "volleyball": {
            toSet.setProductionType(Sport.VOLLEYBALL);
            break;
        }
        case "tennis": {
            toSet.setProductionType(Sport.TENNIS);
            break;
        }
        case "all": {
            toSet.setProductionType(Sport.ALL);
            break;
        }
        }

    }

}
