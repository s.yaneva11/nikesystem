package commands;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        NikeSystem ns = new NikeSystem();
        ArrayList<String> argv = new ArrayList<>();
        Executor e = new Executor();

        @SuppressWarnings("resource")
        Scanner input = new Scanner(System.in);

        while (input.hasNext()) {
            String text = input.nextLine();
            argv = e.splitInput(text);
            ns.run(argv);

        }
    }

}
