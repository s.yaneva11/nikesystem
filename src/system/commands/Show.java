package commands;

import java.util.ArrayList;

import hashing.Hash;
import users.User;

public class Show extends Command {

    private String username;

    public Show(String username) {
        super();
        this.username = username;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {
        boolean flag = system.userExists(username);
        if (flag) {
            User toShow = new User();
            toShow = system.getCurentUser(username);
            System.out.println("Username: " + toShow.getUsername());

            if (toShow.getTypeOfUser().equals("admin")) {
                Hash hashed = new Hash();
                String hashedPassword = hashed.getHash(toShow.getPassword());
                System.out.println("Password: " + hashedPassword);
            }

            System.out.println("Type of user: " + toShow.getTypeOfUser());
        } else {
            throw new IllegalArgumentException("No such user!");
        }

    }

}
