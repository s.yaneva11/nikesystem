package commands;

import java.util.ArrayList;

import users.User;

public class LogOut extends Command {

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {
        User guest = new User();
        system.setCurrentUser(guest);
        System.out.println("You are now using the system as guest user.");
    }

}
