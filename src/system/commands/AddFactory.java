package commands;

import java.util.ArrayList;

import factory.Factory;

public class AddFactory extends Command {
    
    private String name;
    
    public AddFactory(String name) {
        super();
        this.name = name;
    }


    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {
        Factory f = new Factory(args.get(2));
        
        boolean flag = system.factoryExists(name);
        
        if(flag) {
            throw new IllegalArgumentException("Factory with such name already exists!");
        }
        
        system.factories.put(name, f);
        System.out.println("Factory added successfully.");
    }

}
