package commands;

import java.util.ArrayList;

public abstract class Command {

    abstract public void runCommand(ArrayList<String> args, NikeSystem system);

}
