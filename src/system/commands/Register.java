package commands;

import java.util.ArrayList;

import users.User;

public class Register extends Command {

    private String username;
    private String password;
    private String type;

    public Register() {
        super();
    }

    public Register(String username, String password, String type) {
        super();
        this.username = username;
        this.password = password;
        this.type = type;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {
        User user = new User(args.get(1), args.get(2), args.get(3));

        boolean flag = system.userExists(username);

        if (flag) {
            throw new IllegalArgumentException("User with such username already exists!");
        }

        system.users.put(username, user);
        System.out.println("User added successfully");
    }

}
