package commands;

import java.util.ArrayList;

import db.DataBase;
import users.User;

public class LogIn extends Command {

    private String username;
    private String password;
    private DataBase db;

    public LogIn() {
        super();
    }

    public LogIn(String username, String password) {
        super();
        this.username = username;
        this.password = password;
        db = new DataBase();
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {

        boolean flag = db.contains(username);

        User helper = new User();
        helper = system.getCurentUser(username);
        String pass = helper.getPassword();

        if (flag || (system.userExists(username) && password.equals(pass))) {
            system.setCurrentUser(system.getCurentUser(username));
        } else {
            throw new IllegalArgumentException("Invalid username or password!");
        }
    }

}
