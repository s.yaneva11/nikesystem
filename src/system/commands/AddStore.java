package commands;

import java.util.ArrayList;
import java.util.Random;

import factory.Store;

public class AddStore extends Command {

    private String name;

    public AddStore(String name) {
        super();
        this.name = name;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {

        boolean flag = system.storeExists(name);

        if (flag) {
            throw new IllegalArgumentException("Store with such name already exists!");
        }

        Store newStore = new Store(name);

        system.stores.add(newStore);
        addProductsToStore(newStore, system);
        System.out.println("Store created successfully.");

    }

    public void addProductsToStore(Store s, NikeSystem ns) {
        Random rand = new Random();
        int n = rand.nextInt(20) + 1;

        for (int i = 0; i < n; i++) {
            Object[] arr = ns.products.keySet().toArray();
            Object key = arr[new Random().nextInt(arr.length)];
            s.addProducts(ns.products.get(key));
        }
    }

}
