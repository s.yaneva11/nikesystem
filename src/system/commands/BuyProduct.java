package commands;

import java.util.ArrayList;
import java.util.Random;

import factory.Factory;
import factory.Product;
import factory.Store;

public class BuyProduct extends Command {

    private String name;

    public BuyProduct(String name) {
        super();
        this.name = name;
    }

    @Override
    public void runCommand(ArrayList<String> args, NikeSystem system) {

        boolean flag = system.productExists(name);

        if (flag) {
            Product toBuy = new Product();
            toBuy = system.getProduct(name);

            if (args.get(2).equals("online")) {
                
                Object[] arr = system.factories.keySet().toArray();
                Object fabric = arr[new Random().nextInt(arr.length)];

                Factory f = new Factory();
                f = system.getFactory(system.factories.get(fabric).getName());
                f.removeProduct(toBuy);

                System.out.println("You bought this product from online (random) factory");
            } else {

                boolean exist = system.storeExists(args.get(2));

                if (exist) {

                    String storeName = args.get(2);
                    Store s = new Store();
                    s = system.getStore(storeName);
                    s.removeProduct(toBuy);

                    System.out.println("You bought this product from" + storeName + " store");
                } else {
                    throw new IllegalArgumentException("No such store!");
                }
            }
        }
        else {
            throw new IllegalArgumentException("Such product does not exist!");
        }
        
    }

}
