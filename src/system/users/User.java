package system.users;

import system.coordinates.Coordinates;

public class User {

    private String username;
    private String password;
    private String typeOfUser;
    private Coordinates coordinates;


    public User() {
        username = "GuestUser";
    }

    public User(String username, String password, String typeOfUser) {
        super();
        this.username = username;
        this.password = password;
        this.typeOfUser = typeOfUser;
        coordinates = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTypeOfUser() {
        return typeOfUser;
    }

    public void setTypeOfUser(String typeOfUser) {
        this.typeOfUser = typeOfUser;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }
    
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
    
}
