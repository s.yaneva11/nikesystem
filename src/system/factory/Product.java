package factory;

public class Product {

    private String name;
    private Sport sport;

    public Product() {
        name = null;
        sport = null;
    }

    public Product(String name, Sport sport) {
        super();
        this.name = name;
        this.sport = sport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

}
