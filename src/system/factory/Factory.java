package factory;

import java.util.HashMap;

public class Factory {

    private String name;
    private Sport productionType;
    private HashMap<String, Product> products;

    public Factory() {
        this.name = null;
        productionType = null;
        products = new HashMap<>();
    }

    public Factory(String name) {
        super();
        this.name = name;
        productionType = null;
        products = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sport getProductionType() {
        return productionType;
    }

    public void setProductionType(Sport productionType) {
        this.productionType = productionType;
    }

    public void addProducts(Product p) {
        products.put(p.getName(), p);
    }

    public void removeProduct(Product p) {
        products.remove(p.getName(), p);
    }

    public void print() {
        for (String key : products.keySet()) {
            System.out.println("Name: " + products.get(key).getName());
            System.out.println("Sport: " + products.get(key).getSport());
        }
    }
}
