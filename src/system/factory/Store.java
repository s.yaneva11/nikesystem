package system.factory;

import java.util.HashMap;

import system.coordinates.Coordinates;

public class Store {

    private String name;
    private HashMap<String, Product> products;
    private Coordinates coordinates;


    public Store() {
        this.name = null;
        products = new HashMap<>();
        coordinates = null;
    }

    public Store(String name) {
        super();
        products = new HashMap<>();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }
    
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
    
    public void addProducts(Product p) {
        products.put(p.getName(), p);
    }

    public void removeProduct(Product p) {
        products.remove(p.getName(), p);
    }
    
    public boolean contains(Product p) {
        for(Product value : products.values()) {
            if(value.equals(p)) {
                return true;
            }
        }
        return false;
    }

    public void print() {
        for (String key : products.keySet()) {
            System.out.println("Name: " + products.get(key).getName());
            System.out.println("Sport: " + products.get(key).getSport());
        }
    }

}
