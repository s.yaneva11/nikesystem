package db;

import java.io.BufferedReader;
import java.io.FileReader;

import java.io.IOException;

public class DataBase {

    private static final String FILENAME = "D:\\database.txt";

    public static String getFilename() {
        return FILENAME;
    }

    public DataBase() {

    }

    public boolean contains(String name) {
        String row = "";
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
            while ((row = br.readLine()) != null) {
                if (row.contains(name)) {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}
